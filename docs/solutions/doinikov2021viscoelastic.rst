.. _Doinikov2021Viscoelastic:

Doinikov (viscoelastic fluid - elastic sphere; 2021)
======================================================

Scattering field only

`Link to paper (acoustic streaming)
<https://journals.aps.org/pre/abstract/10.1103/PhysRevE.104.065107>`_

.. inheritance-diagram::
    osaft.solutions.doinikov2021viscoelastic.scattering
   :top-classes: osaft.solutions.base_solution.BaseSolution
   :parts: 1

.. currentmodule:: osaft.solutions.doinikov2021viscoelastic

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~scattering.ScatteringField

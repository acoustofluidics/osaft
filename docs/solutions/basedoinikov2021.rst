.. _BaseDoinikov2021:

Doinikov 2021 Base Solution
=================================

This is not an actual solution but only provides base classes for
:ref:`Doinikov2021Viscous` and :ref:`Doinikov2021Viscoelastic`.

.. inheritance-diagram::
    osaft.solutions.basedoinikov2021.scattering
   :top-classes: osaft.solutions.base_solution.BaseSolution
   :parts: 1

.. currentmodule:: osaft.solutions.basedoinikov2021

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~base.BaseDoinikov
   ~coefficientmatrix.CoefficientMatrix
   ~scattering.BaseScatteringDoinikov2021

.. _BaseStreaming:

Base Streaming
=====================

.. autoclass:: osaft.solutions.base_streaming.BaseStreaming
   :members:
   :special-members:
   :show-inheritance:

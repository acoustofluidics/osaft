.. _BaseSolution:

Base Solution
=====================

.. autoclass:: osaft.solutions.base_solution.BaseSolution
   :members:
   :special-members:
   :show-inheritance:

.. _Doinikov1994Rigid:

Doinikov (viscous fluid - rigid sphere; 1994)
=============================================


`Link to paper
<https://royalsocietypublishing.org/doi/abs/10.1098/rspa.1994.0150>`_

.. inheritance-diagram::
    osaft.solutions.doinikov1994rigid.arf
   :top-classes: osaft.solutions.base_arf.BaseARF
   :parts: 1

.. currentmodule:: osaft.solutions.doinikov1994rigid

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~base.BaseDoinikov1994Rigid
   ~scattering.ScatteringField
   ~arf.ARF

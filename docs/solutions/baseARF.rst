.. _BaseARF:

Base ARF
=====================

.. autoclass:: osaft.solutions.base_arf.BaseARF
   :members:
   :special-members:
   :show-inheritance:

.. _BaseScattering:

Base Scattering
======================

.. autoclass:: osaft.solutions.base_scattering.BaseScattering
   :members:
   :special-members:
   :show-inheritance:

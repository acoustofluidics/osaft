.. _Yosioka1955:

Yosioka & Kawasima (1955)
=========================

`Link to paper
<https://www.ingentaconnect.com/content/dav/aaua/1955/00000005/00000003/art00004>`_

.. inheritance-diagram::
    osaft.solutions.yosioka1955.arf
   :top-classes: osaft.solutions.base_arf.BaseARF
   :parts: 1

.. currentmodule:: osaft.solutions.yosioka1955

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~base.BaseYosioka
   ~scattering.ScatteringField
   ~arf.ARF

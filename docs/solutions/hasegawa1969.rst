.. _Hasegawa1969:

Hasegawa & Yosioka (1969, 1979)
===============================

This solution implements the theory from the 1969 paper from Hasegawa and
Yosioka and the standing wave solution from the 1979 paper of Hasegawa.

`Link to paper (1969)
<https://asa.scitation.org/doi/10.1121/1.1911832>`_

`Link to paper (1979)
<https://asa.scitation.org/doi/abs/10.1121/1.382263>`_

.. inheritance-diagram::
    osaft.solutions.hasegawa1969.arf
   :top-classes: osaft.solutions.base_arf.BaseARF
   :parts: 1

.. currentmodule:: osaft.solutions.hasegawa1969

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~base.BaseHasegawa
   ~scattering.ScatteringField
   ~arf.ARF

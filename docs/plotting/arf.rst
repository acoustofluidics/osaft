ARF Plots
=====================

.. currentmodule:: osaft.plotting.arf

.. autosummary::
   :toctree: generated/
   :template: template_gallery.rst
   :nosignatures:

   ~arf_plots.ARFPlot

..
   this creates automatically links to all the classes; the naming is e.g. for
   the ActiveVariable --> ref-src.core.variable-ActiveVariable

.. _ref-{{module}}-{{name}}:

{{ name | escape | underline}}

.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}
   :show-inheritance:
   :members:
   :inherited-members:


   .. autoclasstoc::

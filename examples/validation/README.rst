.. _Examples_validation:

Examples from original articles
-------------------------------

The following examples are re-creations from the publication that were the
foundation for the implemented models.
